package com.example.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exam1 {

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 5, 6, 2, 4};
        System.out.println(searchIndex(arr, 4));
    }

    public static List<Integer> searchIndex(int[] arr, int value) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value)
                result.add(i);
        }
        return (result.isEmpty()) ? Collections.singletonList(-1) : result;
    }
}
